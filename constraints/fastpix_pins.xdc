set_property IOSTANDARD LVDS_25 [get_ports gpio_*]

#MB0 - gpio 0
set_property PACKAGE_PIN R25 [get_ports {gpio_p[0]}]
set_property PACKAGE_PIN R26 [get_ports {gpio_n[0]}]

#MB1 - gpio 1
set_property PACKAGE_PIN T30 [get_ports {gpio_p[1]}]
set_property PACKAGE_PIN U30 [get_ports {gpio_n[1]}]

#MB2 - gpio 2
set_property PACKAGE_PIN R28 [get_ports {gpio_p[2]}]
set_property PACKAGE_PIN T28 [get_ports {gpio_n[2]}]

#MB3 - gpio 3
set_property PACKAGE_PIN V28 [get_ports {gpio_p[3]}]
set_property PACKAGE_PIN V29 [get_ports {gpio_n[3]}]

#MB4 - gpio 4
set_property PACKAGE_PIN W25 [get_ports {gpio_p[4]}]
set_property PACKAGE_PIN W26 [get_ports {gpio_n[4]}]

#EN_PRE0 - gpio 5
set_property PACKAGE_PIN P25 [get_ports {gpio_p[5]}]
set_property PACKAGE_PIN P26 [get_ports {gpio_n[5]}]

#EN_PRE1 - gpio 6
set_property PACKAGE_PIN T29 [get_ports {gpio_p[6]}]
set_property PACKAGE_PIN U29 [get_ports {gpio_n[6]}]

#XTRA_HBRIDGE - gpio 7
set_property PACKAGE_PIN P30 [get_ports {gpio_p[7]}]
set_property PACKAGE_PIN R30 [get_ports {gpio_n[7]}]


#set_property PACKAGE_PIN AB6 [get_ports mgt_rx_p]
#set_property PACKAGE_PIN AB5 [get_ports mgt_rx_n]
#set_property IOSTANDARD ??? [get_ports mgt_rx_p]
#set_property IOSTANDARD ??? [get_ports mgt_rx_n]

#set_property PACKAGE_PIN Y2 [get_ports mgt_tx_p]
#set_property PACKAGE_PIN Y1 [get_ports mgt_tx_n]
#set_property IOSTANDARD ??? [get_ports mgt_tx_p]
#set_property IOSTANDARD ??? [get_ports mgt_tx_n]

#set_property PACKAGE_PIN W8 [get_ports mgt_refclk_p]
#set_property PACKAGE_PIN W7 [get_ports mgt_refclk_n]
#set_property IOSTANDARD ??? [get_ports mgt_refclk_p]
#set_property IOSTANDARD ??? [get_ports mgt_refclk_n]

set_property PACKAGE_PIN AD18 [get_ports user_sma_clk_p]
set_property PACKAGE_PIN AD19 [get_ports user_sma_clk_n]
set_property IOSTANDARD LVDS_25 [get_ports user_sma_clk_p]
set_property IOSTANDARD LVDS_25 [get_ports user_sma_clk_n]
#set_property DIFF_TERM true [get_ports user_sma_clk_*]

set_property PACKAGE_PIN AB29 [get_ports FMC_LPC_LA30_P]
set_property PACKAGE_PIN AB30 [get_ports FMC_LPC_LA30_N]
set_property IOSTANDARD LVDS_25 [get_ports FMC_LPC_LA30_P]
set_property IOSTANDARD LVDS_25 [get_ports FMC_LPC_LA30_N]
set_property DIFF_TERM true [get_ports FMC_LPC_LA30_*]

set_property PACKAGE_PIN AH28 [get_ports FMC_LPC_LA21_P]
set_property PACKAGE_PIN AH29 [get_ports FMC_LPC_LA21_N]
set_property IOSTANDARD LVDS_25 [get_ports FMC_LPC_LA21_P]
set_property IOSTANDARD LVDS_25 [get_ports FMC_LPC_LA21_N]
set_property DIFF_TERM true [get_ports FMC_LPC_LA21_*]

set_property PACKAGE_PIN AA15 [get_ports FMC_LPC_LA07_P]
set_property PACKAGE_PIN AA14 [get_ports FMC_LPC_LA07_N]
set_property IOSTANDARD LVDS_25 [get_ports FMC_LPC_LA07_P]
set_property IOSTANDARD LVDS_25 [get_ports FMC_LPC_LA07_N]
set_property DIFF_TERM true [get_ports FMC_LPC_LA07_*]


set_property PACKAGE_PIN AE12 [get_ports FMC_LPC_LA02_P]
set_property PACKAGE_PIN AF12 [get_ports FMC_LPC_LA02_N]
set_property IOSTANDARD LVDS_25 [get_ports FMC_LPC_LA02_P]
set_property IOSTANDARD LVDS_25 [get_ports FMC_LPC_LA02_N]
set_property DIFF_TERM true [get_ports FMC_LPC_LA02_*]

#set_false_path -from caribou_fastpix_top_i/xil_ibufds_0/inst/O
#set_false_path -from tdc_clk -to axi_clk


#set_property IOSTANDARD LVDS_25 [get_ports TLU_TRG_*]
#set_property IOSTANDARD LVDS_25 [get_ports TLU_BUSY_*]
set_property IOSTANDARD LVDS_25 [get_ports TLU_T0_*]
set_property IOSTANDARD LVDS_25 [get_ports TLU_CLK_*]
set_property IOSTANDARD LVDS_25 [get_ports fmc_clk_*]
set_property IOSTANDARD LVDS_25 [get_ports FMC_REF_CLK_*]


set_property PACKAGE_PIN AE22 [get_ports fmc_clk_p]
set_property PACKAGE_PIN AF22 [get_ports fmc_clk_n]
set_property DIFF_TERM true [get_ports fmc_clk_*]

#set_property PACKAGE_PIN AH23 [get_ports TLU_TRG_P]
#set_property PACKAGE_PIN AH24 [get_ports TLU_TRG_N]
#set_property DIFF_TERM true [get_ports TLU_TRG_*]

#set_property PACKAGE_PIN AD21 [get_ports TLU_BUSY_P]
#set_property PACKAGE_PIN AE21 [get_ports TLU_BUSY_N]

set_property PACKAGE_PIN AA22 [get_ports TLU_T0_P]
set_property PACKAGE_PIN AA23 [get_ports TLU_T0_N]
set_property DIFF_TERM true [get_ports TLU_T0_*]

set_property PACKAGE_PIN AG21 [get_ports TLU_CLK_P]
set_property PACKAGE_PIN AH21 [get_ports TLU_CLK_N]
set_property DIFF_TERM true [get_ports TLU_CLK_*]

set_property PACKAGE_PIN U25 [get_ports FMC_REF_CLK_P]
set_property PACKAGE_PIN V26 [get_ports FMC_REF_CLK_N]

set_property BITSTREAM.CONFIG.USR_ACCESS TIMESTAMP [current_design]
set_property BITSTREAM.GENERAL.JTAG_XADC Enable [current_design] 


#set_clock_groups -asynchronous -group [get_clocks -of_objects [get_pins caribou_fastpix_top_i/clk_wiz_0/inst/plle2_adv_inst/CLKOUT0]] -group [get_clocks clk_out2_caribou_fastpix_top_clk_wiz_0_0]


create_clock -period 3.226 -name fmc_clk_p -waveform {0.000 1.613} [get_ports fmc_clk_p]

set_false_path -from [get_clocks -of_objects [get_pins caribou_fastpix_top_i/clk_wiz_0/inst/plle2_adv_inst/CLKOUT0]] -to [get_clocks fmc_clk_p]

set_false_path -from [get_clocks -of_objects [get_pins caribou_fastpix_top_i/clk_wiz_0/inst/plle2_adv_inst/CLKOUT0]] -to [get_clocks -of_objects [get_pins caribou_fastpix_top_i/clk_wiz_0/inst/plle2_adv_inst/CLKOUT1]]

set_false_path -from [get_clocks -of_objects [get_pins caribou_fastpix_top_i/clk_wiz_0/inst/plle2_adv_inst/CLKOUT1]] -to [get_clocks fmc_clk_p]
set_false_path -from [get_clocks fmc_clk_p] -to [get_clocks -of_objects [get_pins caribou_fastpix_top_i/clk_wiz_0/inst/plle2_adv_inst/CLKOUT0]]
