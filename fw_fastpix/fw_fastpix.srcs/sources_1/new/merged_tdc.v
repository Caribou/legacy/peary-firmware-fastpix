`timescale 1ns / 1ps

module merged_tdc
#(
    parameter N=8,
    parameter COARSE_LEN=29,
    localparam C_S_AXI_DATA_WIDTH = 32,
    localparam C_S_AXI_ADDR_WIDTH = 7
) (
        input wire [6:0] ext_signal_in,
        output wire [31:0] clock_div,

        input wire t0,

        input wire tdc_clk,
        input wire axi_clk,

        //ADC0_axi_reset         : in std_logic;
        input wire m1_axi_reset,

        input wire m1_axi_awready,  //   : in std_logic;
        output wire [31:0] m1_axi_awaddr, //        : out std_logic_vector(31 downto 0);--32bit-->28bit: top.vhd assign high 4 bit
        output wire m1_axi_awvalid, //    : out std_logic;     
        output wire [7:0] m1_axi_awlen, //        : out std_logic_vector(7 downto 0);
        output wire [2:0] m1_axi_awsize, //         : out std_logic_vector(2 downto 0);
        output wire [1:0] m1_axi_awburst, //    : out std_logic_vector(1 downto 0);
        output wire [3:0] m1_axi_awcache, //    : out std_logic_vector(3 downto 0);
        output wire [2:0] m1_axi_awprot, //        : out std_logic_vector(2 downto 0);
        input wire m1_axi_wready, //        : in std_logic;
        output wire [63:0] m1_axi_wdata, //        : out std_logic_vector(63 downto 0);
        output wire m1_axi_wvalid, //        : out std_logic;
        output wire [7:0] m1_axi_wstrb, //        : out std_logic_vector(7 downto 0);
        output wire m1_axi_wlast, //        : out std_logic;
        input wire m1_axi_bvalid, //        : in std_logic; 
        input wire [1:0] m1_axi_bresp, //        : in std_logic_vector(1 downto 0);
        output wire m1_axi_bready, //        : out std_logic;


		input wire S_AXI_ACLK, //	: in std_logic;
		input wire S_AXI_ARESETN, //	: in std_logic;
		input wire [C_S_AXI_ADDR_WIDTH-1:0] S_AXI_AWADDR,	// : in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		input wire [2:0] S_AXI_AWPROT, //	: in std_logic_vector(2 downto 0);
		input wire S_AXI_AWVALID, //	: in std_logic;
		output wire S_AXI_AWREADY, //	: out std_logic;
		input wire [C_S_AXI_DATA_WIDTH-1:0] S_AXI_WDATA, //	: in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		input wire [(C_S_AXI_DATA_WIDTH/8)-1:0] S_AXI_WSTRB, //	: in std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
		input wire S_AXI_WVALID, //	: in std_logic;
		output wire S_AXI_WREADY, //	: out std_logic;
		output wire [1:0] S_AXI_BRESP, //	: out std_logic_vector(1 downto 0);
		output wire S_AXI_BVALID, //	: out std_logic;
		input wire S_AXI_BREADY, //	: in std_logic;
		input wire [C_S_AXI_ADDR_WIDTH-1:0] S_AXI_ARADDR, //	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		input wire [2:0] S_AXI_ARPROT, //	: in std_logic_vector(2 downto 0);
		input wire S_AXI_ARVALID, //	: in std_logic;
		output wire S_AXI_ARREADY, //	: out std_logic;
		output wire [C_S_AXI_DATA_WIDTH-1:0] S_AXI_RDATA, //	: out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		output wire [1:0] S_AXI_RRESP, //	: out std_logic_vector(1 downto 0);
		output wire S_AXI_RVALID, //	: out std_logic;
		input wire S_AXI_RREADY, //	: in std_logic

        input wire aresetn
    );

wire tdc_enable;

wire [N-1:0] axi_tvalid_;
wire [N*128-1:0] axi_tdata_;
wire [N-1:0] axi_tready_;
wire [N-1:0] axi_tlast_;


reg [1:0] tdc_enable_r;
reg [1:0] fifo_reset_r;
reg [1:0] cnt_reset_r;

always @(posedge tdc_clk) begin
    tdc_enable_r <= {tdc_enable_r[0], tdc_enable};
    fifo_reset_r <= {fifo_reset_r[0], fifo_reset};
    cnt_reset_r <= {cnt_reset_r[0], cnt_reset};
end

wire [N-1:0] tdc_signal_in;
wire [7:0] signal_in;
wire [31:0] input_select;
wire sw_signal_in;
wire [N-1:0] tdc_overflow;

assign signal_in = {sw_signal_in, ext_signal_in};
//assign signal_in = ext_signal_in;

/*assign tdc_signal_in[0] = signal_in[input_select[3:0]];
assign tdc_signal_in[1] = signal_in[input_select[7:4]];
assign tdc_signal_in[2] = signal_in[input_select[11:8]];
assign tdc_signal_in[3] = signal_in[input_select[15:12]];
assign tdc_signal_in[4] = signal_in[input_select[19:16]];
assign tdc_signal_in[5] = signal_in[input_select[23:20]];
assign tdc_signal_in[6] = signal_in[input_select[27:24]];
assign tdc_signal_in[7] = signal_in[input_select[31:28]];*/

assign tdc_signal_in[0] = signal_in[input_select[2:0]];
assign tdc_signal_in[1] = signal_in[input_select[6:4]];
assign tdc_signal_in[2] = signal_in[input_select[10:8]];
assign tdc_signal_in[3] = signal_in[input_select[14:12]];
assign tdc_signal_in[4] = signal_in[input_select[18:16]];
assign tdc_signal_in[5] = signal_in[input_select[22:20]];
assign tdc_signal_in[6] = signal_in[input_select[26:24]];
assign tdc_signal_in[7] = signal_in[input_select[30:28]];

wire t0_pulse;

edge_sync t0_sync_inst (
    .clk(tdc_clk),
    .s_in(t0),
    .s_out(t0_pulse)
);


tdc tdc_inst[N-1:0] (
    .signal_in(tdc_signal_in),
    .tdc_clk(tdc_clk),
    .tdc_enable(tdc_enable_r[1]),
    .t0(t0_pulse),
    .overflow(tdc_overflow),

    .axi_tvalid(axi_tvalid_),
    .axi_tdata(axi_tdata_),
    .axi_tready(axi_tready_),
    .axi_tlast(axi_tlast_),
    .axi_clk(axi_clk),
        
    .fifo_reset(fifo_reset_r[1]),
    .cnt_reset(cnt_reset_r[1])
);

reg act = 0;
reg [2:0] sel = 0;
reg [3:0] cnt = 0;

wire wr_en;
wire [127:0] din;
wire axi_fifo_full;

always @(posedge axi_clk) begin
    if (act & axi_tvalid_[sel]) begin
        if(~axi_fifo_full) begin
            cnt <= cnt - 1;
            
            if (cnt == 0) begin
                act <= 1'b0;
            end
        end
    end else if (|axi_tvalid_ && ~axi_fifo_full) begin
        casez(axi_tvalid_)
            32'b00000001: sel <= 0;
            32'b0000001?: sel <= 1;
            32'b000001??: sel <= 2;
            32'b00001???: sel <= 3;
            32'b0001????: sel <= 4;
            32'b001?????: sel <= 5;
            32'b01??????: sel <= 6;
            32'b1???????: sel <= 7;
        endcase

        act <= 1'b1;
        cnt <= 4'h1;
    end else begin
        act <= 1'b0;
    end
end

assign wr_en = axi_tvalid_[sel] & act & ~axi_fifo_full;


wire [127:0] din_;

assign din_ = axi_tdata_[sel*128 +:128];
//assign din = (cnt == 7) ? {sel, din_[28:0]} : din_;
assign din = (cnt == 1) ? {sel, din_[124:0]} : din_;
//assign din = din_;

generate
    for(genvar i=0; i<N; i=i+1) begin
        assign axi_tready_[i] = wr_en & (sel == i) & ~axi_fifo_full;
    end
endgenerate



assign m1_axi_awaddr = {5'b00110, m1_axi_awaddr_out[26:0]}; // 0x3000_0000

wire [31:0] m1_axi_awaddr_out;
wire [31:0] burst_addr;
wire [31:0] tdc_reset;
wire addr_reset;
wire fifo_reset;
wire cnt_reset;

assign addr_reset = tdc_reset[0];
assign fifo_reset = tdc_reset[1];
assign cnt_reset = tdc_reset[2];

wire [9:0] fifo_wrcnt;
wire [31:0] overflow;

reg axi_overflow;

always @(posedge axi_clk) begin
    if(cnt_reset) begin
        axi_overflow <= 1'b0;
    end else begin
        if(axi_fifo_full) begin
            axi_overflow <= 1'b1;
        end else begin
            axi_overflow <= axi_overflow;
        end
    end
end

assign overflow = {23'h0, axi_overflow, tdc_overflow}; //fixme

axi_mburst #(
    .start_addr(28'h000_0000),
    .end_addr(28'h7FF_FFFF)
    ) axi_mburst_inst (
    .axi_clk(axi_clk), 	//	: in std_logic;
    .axi_reset(m1_axi_reset), 	//	: in std_logic;
    .addr_rst(addr_reset),    //    : in std_logic;
	 
    .fifo_reset(fifo_reset),  //    : in std_logic;       
    .fifo_wrclk(axi_clk),  //    : in std_logic;
    .fifo_data(din),   //    : in std_logic_vector(127 downto 0);
    .fifo_wren(wr_en),   //    : in std_logic;
    .fifo_wrcnt(fifo_wrcnt),  //    : out std_logic_vector(9 downto 0);  
    .burst_addr(burst_addr),  //    : out std_logic_vector(31 downto 0);
    .axi_fifo_full(axi_fifo_full),
       	 
    .axi_awready(m1_axi_awready), //	: in std_logic;
    .axi_awaddr(m1_axi_awaddr_out), 	//    : out std_logic_vector(31 downto 0);--32bit-->28bit: top.vhd assign high 4 bit
    .axi_awvalid(m1_axi_awvalid), //	: out std_logic;	 
    .axi_awlen(m1_axi_awlen), 	//	: out std_logic_vector(7 downto 0);
    .axi_awsize(m1_axi_awsize), 	//    : out std_logic_vector(2 downto 0);
    .axi_awburst(m1_axi_awburst), //	: out std_logic_vector(1 downto 0);
    .axi_awcache(m1_axi_awcache), //	: out std_logic_vector(3 downto 0);
    .axi_awprot(m1_axi_awprot), //	    : out std_logic_vector(2 downto 0);
 
    .axi_wready(m1_axi_wready), //	    : in std_logic;
    .axi_wdata(m1_axi_wdata), //	    : out std_logic_vector(63 downto 0);
	.axi_wvalid(m1_axi_wvalid), //	    : out std_logic;
    .axi_wstrb(m1_axi_wstrb), //		: out std_logic_vector(7 downto 0);
    .axi_wlast(m1_axi_wlast), //		: out std_logic;

    .axi_bvalid(m1_axi_bvalid), //	    : in std_logic; 
    .axi_bresp(m1_axi_bresp), //		: in std_logic_vector(1 downto 0);
    .axi_bready(m1_axi_bready) //	    : out std_logic
);

ADC_Config_v1_0_S00_AXI adc_config_inst (
    .S_AXI_ACLK(S_AXI_ACLK),
    .S_AXI_ARESETN(S_AXI_ARESETN),
    .S_AXI_AWADDR(S_AXI_AWADDR),
    .S_AXI_AWPROT(S_AXI_AWPROT),
    .S_AXI_AWVALID(S_AXI_AWVALID),
    .S_AXI_AWREADY(S_AXI_AWREADY), 
    .S_AXI_WDATA(S_AXI_WDATA),
    .S_AXI_WSTRB(S_AXI_WSTRB),
    .S_AXI_WVALID(S_AXI_WVALID),
    .S_AXI_WREADY(S_AXI_WREADY),
    .S_AXI_BRESP(S_AXI_BRESP),
    .S_AXI_BVALID(S_AXI_BVALID),
    .S_AXI_BREADY(S_AXI_BREADY),
    .S_AXI_ARADDR(S_AXI_ARADDR),
    .S_AXI_ARPROT(S_AXI_ARPROT),
    .S_AXI_ARVALID(S_AXI_ARVALID),
    .S_AXI_ARREADY(S_AXI_ARREADY),
    .S_AXI_RDATA(S_AXI_RDATA),
    .S_AXI_RRESP(S_AXI_RRESP),
    .S_AXI_RVALID(S_AXI_RVALID),
    .S_AXI_RREADY(S_AXI_RREADY),

    .tdc_reset(tdc_reset),
    .burst_addr(burst_addr),
    .tdc_enable(tdc_enable),
    .fifo_wrcnt(fifo_wrcnt),
    .input_select(input_select),
    .sw_signal(sw_signal_in),
    .clock_div(clock_div),
    .overflow(overflow)
);

endmodule
