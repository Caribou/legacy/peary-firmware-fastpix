`timescale 1ns / 1ps

module delay_line 
#(parameter N=126) 
(
    input wire signal_in,
    output wire signal_out,
    output wire [N*4-1:0] data_out,

    input wire clk
);

wire [N*4-1:0] carry_out_data;
wire [N-1:0] carry_in;
wire [N-1:0] carry_out;

delay delay_inst[N-1:0] (
    .signal_in(carry_in),
    .signal_out(carry_out),
    .data_out(carry_out_data),
    .clk(clk)
);

assign carry_in[0] = signal_in;
assign signal_out = carry_out[N-1];

generate
    for(genvar i=0; i<N-1; i=i+1) begin
        assign carry_in[i+1] = carry_out[i];
    end
endgenerate

assign data_out = carry_out_data;

endmodule
