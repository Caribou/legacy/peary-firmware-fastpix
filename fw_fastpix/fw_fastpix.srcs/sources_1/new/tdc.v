`timescale 1ns / 1ps

// 56*4 = 224 bins
// 128 - 224*3/7 = 32 

module tdc
#(
    parameter N=126,
    parameter COARSE_LEN=29
) (
        input wire signal_in,
        output wire signal_out,
        input wire tdc_clk,

        input wire tdc_enable,

        output wire axi_tvalid,
        output wire [127:0] axi_tdata,
        input wire axi_tready,
        output wire axi_tlast,
        input wire axi_clk,

        input wire fifo_reset
    );

wire [N*4-1:0] data;

delay_line dl_inst(.signal_in(signal_in), .signal_out(signal_out), .data_out(data), .clk(tdc_clk));

reg [COARSE_LEN-1:0] coarse_count;

reg [1:0] fifo_reset_r;

always @(posedge tdc_clk) begin
    fifo_reset_r <= {fifo_reset_r[0], fifo_reset};
end

always @(posedge tdc_clk) begin
    if(fifo_reset_r[1]) begin
        coarse_count <= 0;
    end else begin
        coarse_count <= coarse_count + 1;
    end
end

wire [N*4*3/7-1:0] sum_out;
reg [N*4*3/7-1:0] sum_r;

wire [N*4/7-1:0] and_out;
reg [N*4/7-1:0] and_r;
wire [N*4/7-1:0] or_out;
reg [N*4/7-1:0] or_r;

sum sum_inst [N*4/7-1:0] (.in(data), .out(sum_out), .and_(and_out), .or_(or_out));

always @(posedge tdc_clk) begin
    sum_r <= sum_out;
    and_r <= and_out;
    or_r <= or_out;
end

/*reg [N*4*3/7-1:0] sum_r1;
reg [N/7-1:0] and_r1;
reg [N/7-1:0] or_r1;

always @(posedge tdc_clk) begin
    sum_r1 <= sum_r;
end

generate
    for(genvar i=0;i<N/7;i=i+1) begin
        always @(posedge tdc_clk) begin
            and_r1[i] <= &and_r[(i+1)*4-1:i*4];
            or_r1[i] <= |or_r[(i+1)*4-1:i*4];
        end
    end
endgenerate*/

reg [N*4*3/7-1:0] sum_r1;
reg and_r1;
reg or_r1;

always @(posedge tdc_clk) begin
    sum_r1 <= sum_r;
    and_r1 <= &and_r;
    or_r1 <= |or_r;
end

reg [N*4*3/7-1:0] sum_r2;
reg and_r2;
reg or_r2;

always @(posedge tdc_clk) begin
    sum_r2 <= sum_r1;
    and_r2 <= and_r1;
    or_r2 <= or_r1;
end


wire hit;
wire empty;

assign hit = (((and_r2 == 0) & (or_r2 == 1)) | (&coarse_count)) & tdc_enable;

assign axi_tvalid = ~empty;

fifo_generator_0 fifo_inst (
  .rst(fifo_reset),                  // input wire rst
  .wr_clk(tdc_clk),            // input wire wr_clk
  .rd_clk(axi_clk),            // input wire rd_clk
  //.din({3'b000, coarse_count, 8'b00000000, sum_r2}),                  // input wire [255 : 0] din
  .din({3'b000, coarse_count, 5'b00000, sum_r2[215:189], 1'b0, sum_r2[188:126], 1'b0, sum_r2[125:63], 1'b0, sum_r2[62:0]}),                  // input wire [255 : 0] din
  .wr_en(hit),              // input wire wr_en
  .rd_en(axi_tready),              // input wire rd_en
  .dout(axi_tdata),                // output wire [31 : 0] dout
  .full(full),                // output wire full
  .empty(empty),              // output wire empty
  .wr_rst_busy(wr_rst_busy),  // output wire wr_rst_busy
  .rd_rst_busy(rd_rst_busy)  // output wire rd_rst_busy
);
   
endmodule
