module sum (
    input wire [6:0] in,
    output wire [2:0] out,

    output wire and_,
    output wire or_
);

assign out = in[0] + in[1] + in[2] + in[3] + in[4] + in[5] + in[6];
assign and_ = out == 7;
assign or_ = |out;

endmodule
