`timescale 1ns / 1ps

module delay (
    input wire signal_in,
    output wire signal_out,
    output wire [3:0] data_out,

    input wire clk
);

wire [3:0] carry_out;

(* RLOC = "X0Y0", DONT_TOUCH="TRUE" *) CARRY4 c4_inst (
    .CI(signal_in),
    .CYINIT(1'h0),
    .DI(4'h0),
    .S(4'b1111),
    .CO(carry_out),
    .O()
);

(* RLOC = "X0Y0", DONT_TOUCH="TRUE" *) reg [3:0] carry_out_r;

always @(posedge clk) begin
    //carry_out_r <= carry_out;
    carry_out_r <= {carry_out[2], carry_out[3], carry_out[0], carry_out[1]};
end

assign data_out = carry_out_r;
assign signal_out = carry_out[3];

endmodule
