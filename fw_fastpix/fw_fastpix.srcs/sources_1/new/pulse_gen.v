module pulse_gen(
    input wire clk,
    input wire [31:0] div,
    output reg pulse
);

reg [31:0] cnt = 0;

always @(posedge clk) begin
    if(cnt == 0) begin
        pulse = ~pulse;
        cnt <= div;
    end else begin
        cnt <= cnt - 1;
    end
end

endmodule