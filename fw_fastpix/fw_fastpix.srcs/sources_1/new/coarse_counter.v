`timescale 1ns / 1ps


module coarse_counter(
        output wire [25:0] count,
        input wire clk
    );
    
reg [25:0] counter;

always @(posedge clk) begin
    counter <= counter + 1;
end

assign count = counter;

endmodule
